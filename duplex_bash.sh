#/bin/bash
#$ -S /bin/bash
#$ -cwd
cwd=$1
bamdir=$2

cd $cwd

mkdir consensus

cd $bamdir

ls *.bam  > $cwd/consensus/files
cd $cwd/consensus
old_IFS=$IFS
IFS=$'\n'
lines=($(cat files)) # array
IFS=$old_IFS

cat files | while read line;do

echo $line

identifier=$(echo $line |  awk '{ gsub(/.bam/, ""); print }')
echo "identifier is" $identifier
cd $cwd/consensus/
mkdir 'Duplex_'$identifier
cd 'Duplex_'$identifier

echo -e "#!/bin/bash \n#$ -cwd \n" > ./'duplex_'$identifier.sh
echo -e "module load bwa/0.7.12 \nmodule load samtools \nmodule load python3" >> ./'duplex_'$identifier.sh
echo -e "\n"  >> ./'duplex_'$identifier.sh

cmd1="python3 $cwd/code/SSCS_maker.py --Ncutoff 0.3 --cutoff 0.7 --infile $cwd/bam_links/$identifier.bam --SSCS_outfile $identifier.sscs.bam &&"
cmd2="samtools view -bu $identifier.sscs.bam | samtools sort - $identifier.sscs.sort &&"
cmd3="samtools index $identifier.sscs.sort.bam &&"

cmd4="python3 $cwd/code/DCS_maker.py --infile $identifier.sscs.bam --DCS_outfile $identifier.dcs.bam &&"
cmd5="samtools view -bu $identifier.dcs.bam | samtools sort - $identifier.dcs.sort &&"
cmd6="samtools index $identifier.dcs.sort.bam"

echo -e $cmd1 >> ./'duplex_'$identifier.sh
echo -e $cmd2 >> ./'duplex_'$identifier.sh
echo -e $cmd3 >> ./'duplex_'$identifier.sh

echo -e $cmd4 >> ./'duplex_'$identifier.sh
echo -e $cmd5 >> ./'duplex_'$identifier.sh
echo -e $cmd6 >> ./'duplex_'$identifier.sh
done

cd $cwd/consensus/
find . -maxdepth 1 -type d -printf '%f\n' -name 'Duplex*'| sed -n '1!p' |sort > dirs

for dir in $(cat dirs);do

cd $dir

qsub *.* 

cd ..

done
